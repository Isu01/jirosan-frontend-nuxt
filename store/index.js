export const state = () => ({
  isPopupOn: false,
  bodyClasses: [],
  browserDetect: null,
  isMaintenance: process.env.NODE_ENV !== 'development'
})

export const mutations = {
  SET_POPUP_STATE(state, payload) {
    state.isPopupOn = payload
  },
  SET_BROWSER(state, payload) {
    state.browserDetect = payload
  },
  UNLOCK_MAINTENANCE(state) {
    state.isMaintenance = false
  },
}

export const getters = {
  browserDetect: (state) => state.browserDetect,
  bodyClasses: (state) => state.bodyClasses,
  isPopupOn: (state) => state.isPopupOn,
  isMaintenance: (state) => state.isMaintenance,
}


export const actions = {
  async nuxtServerInit({ dispatch, commit }, { error }) {
    // maybe use axios.all  to improve prefo ?
    // if no config fail load page !
    let continueLoad = false;
    
    await dispatch('config/loadConfig').then((e) => {
      continueLoad = true
  }).catch(err => {
      console.log('Error Init:', err)
      commit('config/SET_CONFIG', {
        data: false,
        statusCode: 502
      })
      error({ statusCode: 502, message: 'Loading error please try again' })
    });

    if(continueLoad){
      await dispatch('menus/loadMenu')
      //await dispatch('filters/loadTaxonomies')
    }
  },
  
  async unlockMaintenance({ commit, getters }, data) {
    //const password = 'getters.maintenancePassword'
    //if (data === password) {
      await commit('UNLOCK_MAINTENANCE')
      return true
    /* } else {
      return false
    } */
  }
}
