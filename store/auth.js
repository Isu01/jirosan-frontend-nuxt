export const state = () => ({
  userLogin: false,
  validationDone: false,
  isLoggedIn: false
})

export const getters = {
  currentUser: (state) => state.userLogin,
  validationDone: (state) => state.validationDone,
  isLoggedIn: (state) => state.isLoggedIn
}

export const mutations = {
  SET_AUTH_DONE(state) {
    state.validationDone = true
  },
  SET_AUTH_STATUS(state, payload) {
    state.isLoggedIn = payload.valid
    state.userLogin = payload.user
  }
}

export const actions = {
  async validateCookie({ commit, rootGetters }, payload) {
    // we can set it right now, no need to trigger it any later
    commit('SET_AUTH_DONE')
    //  http://simonsen.wp/api/auth/
    const siteUrl =
      rootGetters['config/getConfig'] && rootGetters['config/getConfig'].siteurl
        ? rootGetters['config/getConfig'].siteurl
        : false
    if (!siteUrl) {
      return null
    }
    const params = { cookie: payload }
    if (process.env.NODE_ENV === 'development') {
      params.insecure = 'cool'
    }
    await this.$axios
      .$get(`${siteUrl}/api/auth/validate_auth_cookie/`, {
        params
      })
      .then((res) => {
        if ( res && res.valid) {
          const user = payload.split('|')[0]
          commit('SET_AUTH_STATUS', { valid: res.valid, user })
        }
      })
  }
}
