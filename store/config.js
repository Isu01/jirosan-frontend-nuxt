export const state = () => ({
  config: null,
  statusCode: 0,
})

export const getters = {
  getConfig: ({config}) => config,
  getConfigStatus: ({statusCode}) => statusCode,
  homeUrl: ({config}) => config.home,
  verCache: ({config}) => config.persistor_key,
  allowedPostTypes: ({config}) => config.post_types,
  backendUrl: ({config}) => config.siteurl + '/wp-admin',
  getOption: ({config}, getters, contex, rootState) => (optionName) => {
    if (optionName && getters.getConfig) {
      if (getters.getConfig.options) {
        return getters.getConfig.options[optionName]
      }
    }
    return null
  },
  getHomeUrl: (state) => (state.config ? state.config.home : '')
}

export const mutations = {
  SET_CONFIG(state, payload) {
    state.config = payload.data
    state.statusCode = payload.statusCode
  }
}

export const actions = {
  /*   loadConfig({ commit, error }) {
    commit('SET_CONFIG', { data: process.env.wpConfig, statusCode: 200 })
  } */
  // Since we're uisng nuxt-i18 - config is from nuxt.config.js
  async loadConfig({ commit }) {
    const now = new Date().getTime()
    const base = process.env.NODE_ENV !== 'development' ? process.env.PROD_BACKEND_API : process.env.DEV_BACKEND_API;
  
    return await this.$axios.$get(base+'/isuapi/cached/config?cache='+ now).then((res) => {
      if(res){
        commit('SET_CONFIG', {
          data: res,
          statusCode: 200
        })
        return res
      }else{
        commit('SET_CONFIG', {
          data: false,
          statusCode: 404
        })
      }
    })
  }
}