import jsonfile from 'jsonfile'

export const state = () => ({
  menus: []
})

export const mutations = {
  SET_MENU(state, payload) {
    state.menus = payload
  }
}
export const getters = {
  getMenu: (state, getters, context, rootState) => (location = 'top-menu') => {
    let menuToReturn = []

    if ( state.menus && state.menus && state.menus[location] ) {
      menuToReturn = state.menus[location]
    }

    return menuToReturn
  }
}

export const actions = {
  async loadMenu({ commit, rootGetters },) {
    const getterCache = rootGetters['config/verCache']
    const cacheVer =  getterCache || new Date().getTime()
    const file = './cache/menus.json'
    let cachedData = null;

/*     if(process.server){
      await jsonfile.readFile(file)
        .then(data => {
          if(data){
            cachedData = JSON.parse(data)
          }
        })
        .catch(error => console.error('noFile, trying generate new, ', error))
    } */

    if(cachedData && cachedData.version && cachedData.version === cacheVer){
      commit('SET_MENU', cachedData.data)
      return cachedData.data
    }
    
    const base = process.env.NODE_ENV !== 'development' ? process.env.PROD_BACKEND_API : process.env.DEV_BACKEND_API;


    return await this.$axios.$get(base+'/isuapi/cached/menus?ver='+cacheVer).then((res) => {
      if (res) {
        /* if(process.server){
          const fileData = {'version': cacheVer, data: res }
          const fileJson = JSON.stringify(fileData);
          jsonfile.writeFile(file, fileJson, function (err) {
            if (err) console.error('problem with  creating file', err)
          })
        } */
        commit('SET_MENU', res)
      }else{
        console.log('issue with res', res)
      }
      return res;
    })

  }
}
