import _uniqBy from 'lodash.uniqby';
import _orderBy from 'lodash.orderby';
import { trimLink, getUrlApi } from '@/helpers/'

export const state = () => ({
  pages: [],
  currentPage: null

})

export const getters = {
  currentPage: ({currentPage}) => currentPage,

  getPages: ({pages}) => (params) =>{
    if(typeof params !== 'object') return null
    const limit = params.limit || 10;
    let pagesToShow = [];
    if(params.type && (typeof params.type !== 'array') && (typeof params.type === 'string')) {
      params.type = [params.type]
    }

    pagesToShow = pages.filter( p => {
      let canShow = true
      if(params.type) canShow = params.type.includes(p.type)
      return canShow
    })

    if(params.orderby && pagesToShow && pagesToShow.length){
      const order = params.order ? params.order.toLowerCase() : 'asc'
      pagesToShow = _orderBy(pagesToShow,  params.orderby, order)
    }
    
    return pagesToShow.slice(0, limit)
  },

  getStoredPage: ({pages}) => (params) => {
    if(!params){
      console.warn('params not provided, provided:', params)
      return null
    }
    if(pages && pages){
      
      const pageFound = pages.filter(e=>{
        let canShow = false
        
        if(params.include){
          canShow = params.include.includes(Number(e.id))
        }else if(params.id){
          canShow = Number(e.id) === Number(params.id)
        }else{
          canShow = (e.slug === params.slug && e.type === params.type)
          if(!process.server){
            //double check based link, due to possibility of child pages
            const path = $nuxt.context.route.path
            if(e.link && path && canShow){
              canShow = trimLink(e.link) === trimLink(path)
            }
          }
        }
        return canShow
      })
      return pageFound || false
    }
    
    return null;

  }
}

export const mutations = {
  STORE_CURRENT_PAGE(state, page){
    state.currentPage = page
  },
  STORE_LOADED_PAGES( state, payload) {
    if(!payload.pages) {
      console.warn('No params to store page')
      return null
    }
    const tmp = [...state.pages, ...payload.pages]
    const filtered = _uniqBy(tmp, 'id');
    state.pages = filtered
  }
}

export const actions = {
  async fetchPages({ commit, rootGetters }, params) {
    //if (!params.type) return false
    //Check if config is loaded
    const getConfigStatus = rootGetters['config/getConfigStatus']
    if(getConfigStatus !== 200) return false
    
    if(!params.action) params.action = 'post'
    //Should we exclude loaded ?
    params.cache =  rootGetters['config/verCache']
    const lang = false

    const apiUrl = getUrlApi()

    return await this.$axios.$get(apiUrl, { params }).then(res => {
      if(res && !res.error && res.posts && res.posts.length){
        commit('STORE_LOADED_PAGES', { pages: res.posts, lang: lang } )
      }
      return res;
    })
  },

  async loadArchives({commit, rootGetters}, paramsPayload) {
    if (!paramsPayload.type) return {}
    const cache =  rootGetters['config/verCache']
    const apiUrl = getUrlApi()

    const params = {...paramsPayload, action: 'archive', cache }
    return await this.$axios.$get(apiUrl, { params })
  },

  async loadPage({ commit, rootGetters }, payload) {
    if (!payload.postType) return false
    const apiUrl = getUrlApi()

    //Check if config is loaded
    const getConfigStatus = rootGetters['config/getConfigStatus']
    if(getConfigStatus !== 200) return false
    
    const getConfig = rootGetters['config/getConfig']

    let params = {
      action: 'post',
      type: payload.postType,
      limit: 1,
      cache: rootGetters['config/verCache']
    }

    if(payload.slug !== '') params.slug = payload.slug 
    
    if(payload.all_events ) params.all_events = true

    if(payload.allow_private ) params.allow_private = true

    if(payload.slug !== '') params.slug = payload.slug 

    if (
      payload.postType === 'page' &&
      payload.slug === '' &&
      getConfig.home_page
    ) {
      const homePageId = getConfig.home_page.id
      params.id = homePageId
    }

    const isStored = await getters.getStoredPage(params);
    if(isStored && isStored[0]){
      if(process.env.NODE_ENV === 'development'){
        console.log('POST STORED')
      }
      return { 'status' : 200, page: isStored[0] }
    }

    return await this.$axios.$get(apiUrl, { params }).then(res => {
      if( !(res && res.posts && res.posts[0]) ){
        return { 'status' : 404 }
      }else{
        if(process.env.NODE_ENV === 'development'){
          console.log('POST AXIOS')
        }
        commit('STORE_LOADED_PAGES', { pages: res.posts } )
        return { 'status' : 200, page: res.posts[0] }
      }
    })
    
  }
}
