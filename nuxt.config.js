
require('dotenv').config()
import serveStatic from 'serve-static'

const apiDomain =
process.env.NODE_ENV === 'development'
? process.env.DEV_BACKEND_API
: process.env.PROD_BACKEND_API
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/static/logo.png' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Righteous&family=Roboto:wght@300;400;700&family=Noto+Sans+JP&display=swap'
      },
    ]
  },
  pwa: {
    manifest: {
      name: 'JiroSan',
      "short_name": "JiroSan",
      "start_url": "/",
      "display": "standalone",
      "background_color": "#0a0a0a",
      "theme_color": "#0a0a0a",
    }
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#811416' },
  /*
  ** Global CSS
  */
  css: [{ src: '@/assets/scss/global.scss', lang: 'sass' }, { src: '@/assets/scss/extends.scss', lang: 'sass' }],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/mixins.js',
    '~/plugins/mixinAuth.js',
    '~/plugins/extendsFunctions.js',
    '~/plugins/isIE.js',
    '~/plugins/gtm.js',
    '~/plugins/youtube.js',
    { src: '~/plugins/vue-lazyload.client', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/gtm',
    '@nuxtjs/moment',
    'cookie-universal-nuxt'
  ],
  moment: {
    defaultLocale: 'pl',
    locales: ['pl']
  },
  //https://github.com/nuxt-community/gtm-module
  gtm: {
    pageTracking: true
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  router: {
    middleware: ['maintenance']
    //middleware: ['redirects']
  },
  serverMiddleware: [
    { path: '/login', handler:  '~/server-middleware/loginHandler.js'},
  ],
  axios: {
    credentials: false,
    proxy: true,
    degut: true
  },
  proxy: {
    "/api":  {
      "target": apiDomain+'/isuapi',
      "secure": process.env.NODE_ENV !== 'development',
      "changeOrigin": true
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, { isDev, isClient }) {
      config.node = {
        fs: 'empty'
      }
      // ....
    },
  }
}
