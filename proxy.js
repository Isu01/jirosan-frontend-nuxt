import express from 'express'
import proxy from 'express-http-proxy'
const app = express()
app.use('/', proxy(`https://backend.jirosan.pl`))
export default app