/**
 * We're using it, not middleware, since middleware is not fired at first client side,
 * we want validate user ONLY on client side, since it is slow, and this application
 * does not need SSR auth, if it ides, then we can use middlware
 */

import Vue from 'vue'
import { mapGetters } from 'vuex'

Vue.mixin({
  mounted() {
    if (!this.validationDone && !process.server) {
      
      const authCookie = this.$cookies.get('cookie-auth')
      const loggedIn = this.$cookies.get('cookie-logged_in')

      if (authCookie && loggedIn) {
        const authCookieExploded = authCookie.split('|')
        const loggedInExploded = loggedIn.split('|')
        if (
          authCookieExploded[0] === loggedInExploded[0] &&
          authCookieExploded[1] === loggedInExploded[1] &&
          authCookieExploded[2] === loggedInExploded[2]
        ) {
          this.$store.dispatch('auth/validateCookie', loggedIn)
        }
      }
    }
  },
  computed: {
    ...mapGetters({
      validationDone: 'auth/validationDone'
    })
  }
})
