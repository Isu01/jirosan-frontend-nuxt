import browser from 'browser-detect'
//https://www.npmjs.com/package/browser-detect
export default function({ store, req }) {
  let result = null
  if(process.server){
    result = browser(req.headers['user-agent']);
  }else{
    result = browser();
  }
  store.commit('SET_BROWSER', result)
}