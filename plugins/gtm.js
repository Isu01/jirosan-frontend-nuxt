export default function({ $gtm, route, store }) {
  const getConfig = store.getters['config/getConfig']
  const getOption = store.getters['config/getOption']

  if(getConfig){
    const GTM = getOption('gtm_tag')
    if(GTM){
      $gtm.init('GTM-'+GTM)
    }
  }
}