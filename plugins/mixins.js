import Vue from 'vue'
import { mapGetters } from 'vuex'

Vue.mixin({
  data(){
    return {
      fixingViewPortFocus: 'width=device-width, initial-scale=1.0, maximum-scale=5.0'
    }
  },
  head () {
    const VM = this
    //console.log('inFocus', VM.fixingViewPortFocus)
    return {
      meta: [
        { 
          name: 'viewport', 
          content: VM.fixingViewPortFocus
        }
      ]
    }
  },
  methods: {
    focusInputFixViewport(inFocus = false){
      if(inFocus){
        this.fixingViewPortFocus = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
      }else{
        this.fixingViewPortFocus = 'width=device-width, initial-scale=1.0, maximum-scale=5.0'
      }
    },
    handleLinkClick ($event) {
      // ensure we use the link, in case the click has been received by a subelement
      let { target } = $event
      while (target && target.tagName !== 'A') target = target.parentNode

      // handle only links that occur inside the component and do not reference external resources
      if (target && target.matches("a[href*='"+this.homeUrl+"']") && target.href) {
        // some sanity checks taken from vue-router:
        // https://github.com/vuejs/vue-router/blob/dev/src/components/link.js#L106
        const { altKey, ctrlKey, metaKey, shiftKey, button, defaultPrevented } = $event
        // don't handle with control keys
        if (metaKey || altKey || ctrlKey || shiftKey) return
        // don't handle when preventDefault called
        if (defaultPrevented) return
        // don't handle right clicks
        if (button !== undefined && button !== 0) return
        // don't handle if `target="_blank"`
        if (target && target.getAttribute) {
          const linkTarget = target.getAttribute('target')
          if (/\b_blank\b/i.test(linkTarget)) return
        }
        // don't handle same page links/anchors

        const url = new URL(target.href)
        const to = url.pathname
        if (window.location.pathname !== to && $event.preventDefault) {
          $event.preventDefault()
          this.$router.push(to)
        }
      }
    }
  },
  computed: {
    ...mapGetters({
      getConfig: 'config/getConfig',
      homeUrl: 'config/homeUrl',
      currentPage: 'pages/currentPage',
    }),
    pageKey(){
      let key = 'ala'
      if(!process.server){
        key = this.$route.fullPath
      }
      return key
    },
  }
})
