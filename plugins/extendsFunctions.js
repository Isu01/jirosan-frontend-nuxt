/**
 * JS Extend string functions for my needs
 */
String.prototype.phoneFormat = function () { return this.replace(/\s/g, ''); }
String.prototype.excerpt = function (length, end = '...') {
    return this
        .length > length ? this.substring(0, length) + end : this;
}
String.prototype.capitalize = function () {
    const string = this
    return string.charAt(0).toUpperCase() + string.slice(1);
}

String.prototype.stripHtml = function () {
    const html = this;
    var doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
}

if (typeof Object.assign != 'function') {
    // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, "assign", {
        value: function assign(target) { // .length of function is 2
            'use strict';
            if (target == null) { // TypeError if undefined or null
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource != null) { // Skip over if undefined or null
                    for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        },
        writable: true,
        configurable: true
    });
}