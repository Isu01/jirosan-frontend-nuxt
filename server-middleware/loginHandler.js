const querystring = require('qs')

export default function (req, res, next) {
  let body = ''
  if(req){
    req.on('data', (data) => {
      body += data
    })
  
    req.on('end', () => {
      req.body = querystring.parse(body) || {}
    })
  }
  
  
  next()
}
