export default function ({ redirect, route, store, app, req}) {
  //let isMaintenance = store.getters.isMaintenance;
  let isMaintenance = store.getters['config/getOption']('maintenance_is_on');
  if(store.getters.isMaintenance === false){
    isMaintenance = false
  }
  if(process.env.NODE_ENV === 'development'){
    isMaintenance = false
  }
  /* let domain = ''
  if(!process.server){
    domain = window.location.href
  }else{
    domain = req.headers.host
  } */

  if(route.path === '/login' || route.path === '/login/'){
    isMaintenance = false
  }
  if(store.getters['auth/isLoggedIn']){
    isMaintenance = false
  }
  
/*   const cookieRes = ''//app.$cookies.get('cookie-maintenance')
  const password = '';
  if( cookieRes && isMaintenance && cookieRes === password ){
    isMaintenance = false;
    store.dispatch('unlockMaintenance')
  } */
  
  if(isMaintenance){
    return redirect('/maintenance')
  }
  if(isMaintenance === false && route.path === '/maintenance'){
    return redirect('/')
  }

 }