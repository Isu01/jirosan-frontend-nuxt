// https://javascript.info/comments

/**
 * on Vercel there is problem with proxy call and nuxt for some reason,
 * so server calls we must use directly instead of proxy calls
 */
export const getUrlApi = () => {
  let url = 'api/'
  if (process.server) {
    const base = process.env.NODE_ENV !== 'development' ? process.env.PROD_BACKEND_API : process.env.DEV_BACKEND_API;
    url = base+'/isuapi/'
  }
  return url
}

/**
 * Function is removing slashes from beginning and end of thestring
 * @param {*} link  string to remove slashes
 * 
 */
export const trimLink = (link) => {
  if(!link) return ''
  const re = new RegExp(/^\/|\/$/g)
  return link.replace(re, '')
}
/**
 * Function is removing slashes from beginning of thestring
 * @param {*} link  string to remove slashes
 * 
 */
export const ltrimLink = (link) => {
  if(!link) return ''
  const re = new RegExp(/^\//g)
  return link.replace(re, '')
}
/**
 * Function is removing slashes from end of thestring
 * @param {*} link  string to remove slashes
 * 
 */
export const rtrimLink = (link) => {
  if(!link) return ''
  const re = new RegExp(/\/$/g)
  return link.replace(re, '')
}

/**
 * 
 * @param { DOM } el element
 * @param { String } className  
 */
export const hasClass = (el, className) => {
  if (el.classList)
      return el.classList.contains(className);
  return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
}

/**
 * 
 * @param { DOM } el element
 * @param { String } className  
 */
export const addClass = (el, className) => {
  if (el.classList)
      el.classList.add(className)
  else if (!hasClass(el, className))
      el.className += " " + className;
}

export const removeClass = (el, className) => {
  if (el.classList)
      el.classList.remove(className)
  else if (hasClass(el, className))
  {
      var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
      el.className = el.className.replace(reg, ' ');
  }
}
/**
 * Get random number between twoo
 * 
 * @param {number} min minimum number between
 * @param {number} max maximum number between
 */
export const getRndInteger = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

/**
 * Check if object is empty
 * 
 * @param {object} obj object to check
 */
export const isEmptyObject = (obj) => {
  return Object.keys(obj).length === 0;
}
/**
 * JS way to capitalize string
 * 
 * @param {string} s Element that has to be capitalized 
 */
export const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
  }
/**
 * Sort array of objects based on another array
 * https://gist.github.com/ecarter/1423674
 */

export const mapOrder = (array, order, key) => {
  array.sort((a, b) => {
    const A = a[key];
    const B = b[key];
    return order.indexOf(A) > order.indexOf(B) ? 1 : -1;
  });
  return array;
}


export const arrayToggle = (collection, item) => {
  var idx = collection.indexOf(item);
  if(idx !== -1) {
    collection.splice(idx, 1);
  } else {
    collection.push(item);
  }
  return collection
}

/***
 * Format otuput array of elements lik
 * Element, some other or another
 * if Object provided, you can make use of key to determien what key shoudl be used
 * if extend, it will return array with el and separator
 * if transform names false, then logic for lower case and capitalize will not be used // usefull for e.g author names
 */
export const formatOutputValues = (values, last = 'og', extend = false, key = 'name', transformNames = true) => {
  
  if(!values) return ''

  const elements = values.map((e, i) => {
    if( (typeof e === "object" || typeof e === 'function') && (e !== null) ){
      if(e[key]){
        let name = '';
        if(transformNames){
          name = (i !==  0) ? e[key].toLowerCase() : capitalize(e[key]);
        }else{
          name = e[key]
        }
        const newObj = {...e, [key] : name}
        return newObj
      }
    }else{
      if(transformNames){
        return (i !==  0) ? e.toLowerCase() : capitalize(e);
      }else{
        return e
      }
    }
  })
  
  
  if(extend){
    return elements.map( (e,i) => {
      const sep = (i ===  (elements.length - 2)) ? ' '+last+' ' : ', '
      return {
          item: e,
          sep: (i ===  (elements.length - 1)) ? '' : sep
        }
    })
  }else{
    return elements.join(', ').replace(/, ([^,]*)$/, ' '+last+' $1')
  }
}


export const handlePageLoad = async (store, route, error, loadParams) =>{

  //console.log(loadParams)
  return await store.dispatch('pages/loadPage', loadParams).then(e =>{
    let isError = false;
    if(!e || e.status === 404) isError = true
    if(e && e.status === 200){
      let pageLink = trimLink(e.page.link)
      let currentLink = trimLink(route.path)

      //fix for § in url
      if(pageLink && pageLink.includes('%c2%a7')){
        pageLink = pageLink.replace(/%c2%a7/g, "§")
      }
      if(currentLink && currentLink.includes('%c2%a7')){
        currentLink = currentLink.replace(/%c2%a7/g, "§")
      }
      /* if(!process.server){
        console.log(route)
      } */

      /* console.log('Route Path',currentLink)
      console.log('Loaded Page Link', pageLink)
 */
      //Check if not HomePage
      if( !(route.fullPath === '/') ){
        isError = pageLink !== currentLink
      }
    }

    if(isError){
      error({ statusCode: 404, message: 'Page not found' })
      store.commit('pages/STORE_CURRENT_PAGE', false)
      return false;
    }else{
      if(e.page.template && e.page.template === 'dark'){
        store.commit('SET_PAGE_HEADER_COLOR', 'dark')
        store.commit('SET_HEADER_TYPE', 'template template-dark')
      }
      if(!process.server){
        //setTimeout(()=>{
          store.commit('pages/STORE_CURRENT_PAGE', e.page)
        //}, 100)
      }else{
          store.commit('pages/STORE_CURRENT_PAGE', e.page)
      }
      
      return e.page
    }
  })
}

/**
 * Function return url from src="" from a string
 * 
 * @param {*} string string with src to obtain
 */
export const getSrcFromString = (string = '') =>{
  if(string){
    const srcWithQuotes = string.match(/src\=([^\s]*)\s/)[1]
    
    return srcWithQuotes.substring(1,srcWithQuotes.length - 1)
  }else{
    return ''
  }
}